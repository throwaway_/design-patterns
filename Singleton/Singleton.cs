﻿using System;

namespace Singleton
{
    public class Singleton
    {
        private static Lazy<Singleton> instance = new Lazy<Singleton>(() => new Singleton());
        public static Singleton Instance => instance.Value;

        private Singleton() { }
    }
}
