﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Singleton.DummyDatabase
{
    public sealed class PopulationDatabase : Database
    {
        private static Lazy<PopulationDatabase> instance = new Lazy<PopulationDatabase>(() => new PopulationDatabase());
        public static PopulationDatabase Instance => instance.Value;
        private PopulationDatabase()
            : base() { }

        public int GetPopulationFor(string cityName) => GetValue(cityName);
        public Dictionary<string, int> GetPopulationFor(params string[] cityNames)
        {
            Dictionary<string, int> citiesWithPop = new Dictionary<string, int>();
            foreach(string city in cityNames)
            {
                citiesWithPop.Add(city, GetValue(city));
            }
            return citiesWithPop;
        }
        public int GetTotalPopulationFor(params string[] cityNames)
        {
            return data.Where(cityData => cityNames.Contains(cityData.Key)).Sum(city => city.Value);
        }
        public void SetPopulationFor(string cityName, int populationSize) => SetValue(cityName, populationSize);
    }
}
