﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Singleton.DummyDatabase
{
    public abstract class Database
    {
        protected string dataPath;
        protected Dictionary<string, int> data;
        
        protected Database()
        {
            Initialize();
        }

        public virtual void Initialize()
        {
            TryLoadData();
        }

        public void SetDataPath(string path)
        {
            if (File.Exists(path))
            {
                dataPath = path;
            }
            LoadData();
        }

        public bool TryLoadData()
        {
            if (string.IsNullOrEmpty(dataPath) == false)
            {
                LoadData();
                return true;
            }
            
            return false;
        }

        public virtual void LoadData()
        {
            var foundData = File.ReadAllLines(dataPath).Select(line => line.Split(",")).ToDictionary(kvp => kvp[0], kvp => int.Parse(kvp[1]));
            data = foundData;
        }

        public virtual void SaveData()
        {
            string serializedData = string.Join(Environment.NewLine, data.Select(kvp => $"{kvp.Key},{kvp.Value}"));
            File.WriteAllText(this.dataPath, serializedData);
        }

        protected void SetValue(string header, int value) => data[header] = value;
        protected int GetValue(string header) => data[header];
    }
}
