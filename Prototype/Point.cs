﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prototype
{
    [Serializable]
    public class Point
    {
        public int X;
        public int Y;

        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        public Point() { }

        public override string ToString()
        {
            return $"X: {X}, Y: {Y}";
        }
    }
}
