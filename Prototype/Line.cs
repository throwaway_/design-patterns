﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prototype
{
    [Serializable]
    public class Line
    {
        public Point Start;
        public Point End;

        public Line(Point start, Point end)
        {
            Start = start;
            End = end;
        }

        public Line() { }

        public override string ToString()
        {
            return $"{nameof(Start)}: {Start.ToString()}, {nameof(End)}: {End.ToString()}";
        }
    }
}
