﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace Prototype
{
    public static class ExtensionMethods
    {
        public static T CloneBin<T>(this T self)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(memoryStream, self);
                memoryStream.Position = 0;
                T copy = (T)formatter.Deserialize(memoryStream);
                return copy;
            }
        }

        public static T CloneJSON<T>(this T self)
        {
            string jsonCopy = JsonConvert.SerializeObject(self);
            return JsonConvert.DeserializeObject<T>(jsonCopy);
        }

        public static T CloneXML<T>(this T self)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(Line));
                xmlSerializer.Serialize(memoryStream, self);
                memoryStream.Position = 0;
                T copy = (T)xmlSerializer.Deserialize(memoryStream);
                return copy;
            }
        }
    }
}
