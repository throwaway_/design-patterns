﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Composite
{
    public interface IValueContainer : IEnumerable<int>
    {

    }

    public class SingleValue : IValueContainer
    {
        public int Value;

        public SingleValue() { }
        public SingleValue(int value)
        {
            Value = value;
        }

        public IEnumerator<int> GetEnumerator()
        {
            yield return this.Value;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

    public class ManyValues : List<int>, IValueContainer
    {
        public ManyValues() { }
        public ManyValues(IEnumerable<int> values) : base(values) { }
    }

    public static class ExtensionMethods
    {
        public static int Sum(this IEnumerable<IValueContainer> containers)
        {
            int result = 0;
            foreach (var c in containers)
                foreach (var i in c)
                    result += i;
            return result;
        }
    }
}
