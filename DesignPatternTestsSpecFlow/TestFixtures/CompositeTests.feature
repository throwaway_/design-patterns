﻿@composite
Feature: CompositeTests

Tests for Composite pattern examples.

//@tag1
Scenario: IValueContainers can be summed correctly
	Given I have a value container with value 2
	And I have a value container with values 1, 3, and 5
	Then the sum of all my containers is 11