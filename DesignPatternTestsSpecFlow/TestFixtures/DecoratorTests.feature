﻿@decorator
Feature: DecoratorTests

@clearCreatedObjects
Scenario Outline: Provided age for creatures must not be negative
	When I try to create a <creature> of age -100 named Fred
	Then Fred's age is greater than or equal to 0
	When I try to create a <creature> of age -1 named Blue
	Then Blue's age is greater than or equal to 0
	When I try to create a <creature> of age 40 named George
	Then George's age is 40
	When I try to create a <creature> of age 0 named Bellevue
	Then Bellevue's age is 0

Examples:
| creature |
| Bird     |
| Lizard   |
| Dragon   |

@clearCreatedObjects
Scenario Outline: Dragon flight output is correct at various ages
	Given I have a dragon of age <age> named FredF
	When I command the creature named FredF to fly
	Then I receive the output <expectedOutput>

Examples: 
| age | expectedOutput |
| 1   | flying         |
| 10  | too old        |
| 100 | too old        |

@clearCreatedObjects
Scenario Outline: Dragon crawl output is correct at various ages
	Given I have a dragon of age <age> named FredC
	When I command the creature named FredC to crawl
	Then I receive the output <expectedOutput>

Examples: 
| age | expectedOutput |
| 0   | too young      |
| 1   | too young      |
| 10  | crawling       |
| 10  | crawling       |