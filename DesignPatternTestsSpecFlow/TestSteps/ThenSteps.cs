﻿using Composite;
using Decorator;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace TestSteps
{
    [Binding]
    public partial class ThenSteps
    {
        ScenarioContext _scenarioContext;

        public ThenSteps(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }

        [Then(@"the sum of all my containers is (\d+)")]
        public void GetSumOfAllContainers(int expectedValue)
        {
            List<IValueContainer> containers = _scenarioContext.Get<List<IValueContainer>>("valueContainers");
            int foundValue = containers.Sum();

            Assert.True(foundValue == expectedValue, $"Expected {expectedValue} but found {foundValue}.");
        }

        [Then(@"I receive the output (.*)")]
        public void ThenIReceiveTheOutputX(string expectedOutput)
        {
            string lastReceivedOutput = _scenarioContext.Get<string>("lastReceivedOutput");
            Assert.AreEqual(expectedOutput, lastReceivedOutput);
        }

        [Then(@"(.*)'s age (is(?: (?:greater|less) than(?: or equal to))?) (\d*)")]
        public void ThenTheCreaturesAgeIsComparedToX(string creatureName, string comparisonOperator, int ageForComparison)
        {
            Creature creature = _scenarioContext.Get<List<Creature>>("creatures").Where(creature => creature.Name == creatureName).FirstOrDefault();
            bool compareEquality = comparisonOperator == "is" || comparisonOperator.Contains("equal");

            if (compareEquality && creature.Age == ageForComparison) { Assert.Pass(); }
            if (comparisonOperator.Contains("greater")) { Assert.True(creature.Age > ageForComparison); }
            else { Assert.True(creature.Age < ageForComparison); }
        }
    }
}
