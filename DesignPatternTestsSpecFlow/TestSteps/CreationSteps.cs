﻿using System;
using System.Collections.Generic;
using System.Text;
using TechTalk.SpecFlow;
using Composite;
using System.Linq;
using NUnit.Framework;
using Utilities;
using Decorator;

namespace TestSteps
{
    [Binding]
    public class CreationSteps
    {
        private ScenarioContext _scenarioContext;

        public CreationSteps(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }

        #region Given

        [Given(@"I have a value container with value(?:s)? ((?:\d(?:, )?)+(?:and \d+)?)")]
        public void CreateValueContainer(string values)
        {
            IValueContainer container;
            if (int.TryParse(values, out int value))
            {
                container = new SingleValue(value);
            }
            else
            {
                container = new ManyValues(values.Replace("and", "").Split(',', StringSplitOptions.RemoveEmptyEntries).Select(value => int.Parse(value)).ToArray());
            }
            if (_scenarioContext.TryGetValue("valueContainers", out List<IValueContainer> containers) == false)
            {
                containers = new List<IValueContainer>();
            }
            containers.Add(container);
            _scenarioContext.Set(containers, "valueContainers");
        }

        #endregion

        #region When

        [Given(@"I have a (.*) of age ((?:-)?\d+) named (.*)")]
        [When(@"I try to create a (.*) of age ((?:-)?\d+)(?: named (.*))?")]
        public void WhenITryToCreateACreature(string type, string age, string name)
        {
            Creature creature = null;
            try
            {
                name = string.IsNullOrEmpty(name) ? type.ToUpper() : name;
                creature = CreateCreature(type, name, int.Parse(age));
            }
            catch
            {
                Echo.Error($"Dragon could not be created with age {age}.");
            }
            
            _scenarioContext.Set<bool>(creature != null, "lastObjectCreatedSuccessfully");
            if (creature != null)
            {
                AddCreatureToScenarioContext(creature);
            }
        }

        #endregion

        #region Then

        [Then(@"the object (is|is not) created successfully")]
        public void ThenTheObjectIsCreatedSuccessfully(string isOrNot)
        {
            bool expectation = isOrNot == "is";
            bool reality = _scenarioContext.Get<bool>("lastObjectCreatedSuccessfully");
            Assert.True(reality == expectation);
        }

        #endregion

        #region Helpers

        internal class CreatureOverflowException : Exception
        {
            internal CreatureOverflowException(string message) : base(message) { }
            internal CreatureOverflowException() : base() { }
        }
        internal class CreatureUnknownException : Exception
        {
            internal CreatureUnknownException(string message) : base(message) { }
            internal CreatureUnknownException() : base() { }
        }

        private Creature CreateCreature(string type, string name, int age)
        {
            switch (type.ToLower())
            {
                case "dragon":
                    return new Dragon(name, age);
                case "bird":
                    return new Bird(name, age);
                case "lizard":
                    return new Lizard(name, age);
                default:
                    throw new CreatureUnknownException("The creature factory doesn't support that creature yet.");
            }
        }

        private void AddCreatureToScenarioContext(Creature creature)
        {
            if (_scenarioContext.TryGetValue<List<Creature>>("creatures", out var creatures) == false)
            {
                creatures = new List<Creature>();
            }

            if (creatures.Any(c => c.Name == creature.Name))
            {
                throw new CreatureOverflowException($"You already have a creature named {creature.Name}, stop adding creatures with that name.");
            }
            creatures.Add(creature);
            _scenarioContext.Set(creatures, "creatures");
        }

        #endregion
    }
}
