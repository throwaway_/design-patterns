﻿using Decorator;
using System.Collections.Generic;
using System.Linq;
using TechTalk.SpecFlow;

namespace TestSteps
{
    [Binding]
    public class ActionSteps
    {
        ScenarioContext _scenarioContext;

        public ActionSteps(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }

        [When(@"I command the creature(?: named (.*))? to (.*)")]
        public void WhenICommandTheCreatureCalledXToAct(string name, string actionName)
        {
            Creature creature = _scenarioContext.Get<List<Creature>>("creatures").Where(creature => creature.Name == name).FirstOrDefault();
            if (creature.Can(actionName, out var action))
            {
                _scenarioContext.Set((string)action.Invoke(creature, null), "lastReceivedOutput");
            }
        }
    }
}
