﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Adapter.Exercise
{
    public static class ExtensionMethods
    {
        public static int Area(this IRectangle rectangle)
        {
            return rectangle.Width * rectangle.Height;
        }
    }
}
