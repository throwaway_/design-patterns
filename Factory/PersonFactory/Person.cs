﻿namespace Factory.PersonFactory
{
    public class Person
    {
        public int Id { get; private set; }
        public string Name { get; private set; }

        private Person(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public class PersonFactory
        {
            private int peopleCreated;

            public PersonFactory(int startingIndex = 0)
            {
                this.peopleCreated = startingIndex;
            }

            public Person CreatePerson(string name)
            {
                Person person = new Person(peopleCreated++, name);
                return person;
            }
        }
    }
}
