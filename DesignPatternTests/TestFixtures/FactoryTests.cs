using System.Collections.Generic;
using NUnit.Framework;
using Utilities;
using PersonFactory = Factory.PersonFactory.Person.PersonFactory;

// feature:
using Factory.PersonFactory;

namespace Tests
{
    [TestFixture]
    internal class FactoryTests
    {
        [SetUp]
        public void Setup()        
        {
            Echo.Info("Welcome the tests.");  // :thumbsup:
        }

        [Test]
        public void PersonFactoryIdCountsUp()
        {
            PersonFactory pF = new PersonFactory(0);
            List<Person> people = new List<Person>();
            
            people.Add(pF.CreatePerson("Marge"));
            people.Add(pF.CreatePerson("Tom"));
            people.Add(pF.CreatePerson("Hermine"));
            people.Add(pF.CreatePerson("Alejandrina"));
            people.Add(pF.CreatePerson("Gregor"));
            
            for (int i = 0; i < people.Count; i++)
            {
                Assert.True(people[i].Id == i);
                Echo.Info($"{i}: {people[i].Name}");
            }
        }
    }
}