﻿using System;
using NUnit.Framework;
using Utilities;

// feature
using Prototype;

namespace Tests
{
    [TestFixture]
    internal class PrototypeTests
    {
        [SetUp]
        public void Setup()
        {
            Echo.Info("Welcome the tests.");  // :thumbsup:
        }

        [Test]
        public void CloneWorksBinTest()
        {
            CloneWorks("binary");
        }

        [Test]
        public void CloneWorksJSONTest()
        {
            CloneWorks("json");
        }

        [Test]
        public void CloneWorksXMLTest()
        {
            CloneWorks("xml");
        }

        void CloneWorks(string cloneType)
        {
            Point startPoint = new Point(0, 0);
            Point endPoint = new Point(7, 7);
            Line line = new Line(startPoint, endPoint);

            Line cloneLine()
            {
                switch (cloneType)
                {
                    case "json":
                        return line.CloneJSON();
                    case "binary":
                        return line.CloneBin();
                    case "xml":
                        return line.CloneXML();
                    default:
                        throw new NotImplementedException();
                }
            }

            Line linePlusFive = cloneLine();
            linePlusFive.Start.X += 5;
            linePlusFive.Start.Y += 5;
            linePlusFive.End.X += 5;
            linePlusFive.End.Y += 5;

            Assert.True(line.Start.X == 0 && line.Start.Y == 0 &&
                        line.End.X == 7 && line.End.Y == 7);

            Assert.True(linePlusFive.Start.X == 5 && linePlusFive.Start.Y == 5 &&
                        linePlusFive.End.X == 12 && linePlusFive.End.Y == 12);
        }
    }
}
