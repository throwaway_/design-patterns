using System.IO;
using NUnit.Framework;
using Utilities;

// feature:
using SingletonExample = Singleton.Singleton;
using Singleton.DummyDatabase;

namespace Tests
{
    [TestFixture]
    internal class SingletonTests
    {
        [SetUp]
        public void Setup()        
        {
            Echo.Info("Welcome the tests.");  // :thumbsup:
        }

        [Test]
        public void SingletonIsSingletonTest()
        {
            var testObject = SingletonExample.Instance;
            var testObject2 = SingletonExample.Instance;

            Assert.AreSame(testObject, testObject2);
        }

        [Test]
        public void PopulationDatabaseIsSingletonTest()
        {
            var testObject = PopulationDatabase.Instance;
            var testObject2 = PopulationDatabase.Instance;

            Assert.AreSame(testObject, testObject2);
        }

        [Test]
        public void PopDatabaseCanLoadData()
        {
            string dummyDataPath = Path.Join(Path.GetFullPath("..\\..\\..\\TestAssets"), "citiesWithPop.txt");
            PopulationDatabase database = PopulationDatabase.Instance;
            database.SetDataPath(dummyDataPath);
            database.TryLoadData();

            Assert.AreEqual(database.GetPopulationFor("Pittsburgh"), 1234567);
        }

        [Test]
        public void PopDatabaseCanSaveData()
        {
            string dummyDataPath = Path.Join(Path.GetFullPath("..\\..\\..\\TestAssets"), "citiesWithPop.txt");
            string copyPath = $"{Path.GetFileNameWithoutExtension(dummyDataPath)}_copy.txt";
            File.Copy(dummyDataPath, copyPath);

            PopulationDatabase database = PopulationDatabase.Instance;
            int newValue = 9999999;
            database.SetDataPath(copyPath);
            database.TryLoadData();

            database.SetPopulationFor("Pittsburgh", newValue);
            database.SaveData();

            database.SetDataPath(dummyDataPath);
            database.TryLoadData();
            int originalValue = database.GetPopulationFor("Pittsburgh");

            database.SetDataPath(copyPath);
            database.TryLoadData();
            Assert.True(database.GetPopulationFor("Pittsburgh") == newValue && originalValue != newValue);

            File.Delete(copyPath);
        }

        [Test]
        public void PopDatabaseCanGetTotal()
        {
            PopulationDatabase database = PopulationDatabase.Instance;
            database.SetDataPath(Path.Join(Path.GetFullPath("..\\..\\..\\TestAssets"), "citiesWithPop.txt"));
            database.TryLoadData();
            int firstPop = database.GetPopulationFor("Pittsburgh");
            int secondPop = database.GetPopulationFor("Portland");

            Assert.AreEqual(database.GetTotalPopulationFor("Pittsburgh", "Portland"), firstPop + secondPop);
        }

        [Test]
        public void SingletonTesterCanCheckIfSingleton()
        {
            
        }
    }
}