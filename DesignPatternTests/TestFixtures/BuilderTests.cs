using System;
using NUnit.Framework;
using Utilities;

// feature:
using Builder;

namespace Tests
{
    [TestFixture]
    internal class BuilderTests
    {
        [SetUp]
        public void Setup()        
        {
            Echo.Info("Welcome the tests.");  // :thumbsup:
        }

        [Test]
        public void CodeBuilderBuilds()
        {
            var cb = new CodeBuilder("HenloWorld");
            var code = cb.Build();

            try
            {
                Echo.Debug(code.ToString());
                Assert.True(code is Code);
            }
            catch (Exception ex)
            {
                string msg = $"{ex}: {ex.Message}";
                Echo.Error(msg);
            }
        }
    }
}