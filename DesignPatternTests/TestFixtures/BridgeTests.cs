﻿using NUnit.Framework;
using Utilities;

// feature
using Bridge.Exercise;
using Autofac;

namespace Tests
{
    [TestFixture]
    internal class BridgeTests
    {
        [SetUp]
        public void Setup()
        {
            Echo.Info("Welcome the tests.");  // :thumbsup:
        }

        [Test]
        public void VectorRendererHasCorrectOutput()
        {
            VectorRenderer renderer = new VectorRenderer();

            Triangle triangle = new Triangle(renderer);
            Square square = new Square(renderer);

            Assert.True(triangle.ToString() == "Drawing Triangle with size 1 as lines.", $"Incorrect output; got: \"{triangle}\"");
            Assert.True(square.ToString() == "Drawing Square with size 1 as lines.", $"Incorrect output; got: \"{square}\"");
        }

        [Test]
        public void RasterRendererHasCorrectOutput()
        {
            RasterRenderer renderer = new RasterRenderer();

            Triangle triangle = new Triangle(renderer);
            Square square = new Square(renderer);

            Assert.True(triangle.ToString() == "Drawing Triangle with size 1 as pixels.", $"Incorrect output; got: \"{triangle}\"");
            Assert.True(square.ToString() == "Drawing Square with size 1 as pixels.", $"Incorrect output; got: \"{square}\"");
        }

        [Test]
        public void VectorRendererHasCorrectOutputDI()
        {
            ContainerBuilder containerBuilder = new ContainerBuilder();
            containerBuilder.RegisterType<VectorRenderer>().As<IRenderer>().SingleInstance();

            RendererHasCorrectOutputDI(containerBuilder, "lines");
        }

        [Test]
        public void RasterRendererHasCorrectOutputDI()
        {
            ContainerBuilder containerBuilder = new ContainerBuilder();
            containerBuilder.RegisterType<RasterRenderer>().As<IRenderer>().SingleInstance();

            RendererHasCorrectOutputDI(containerBuilder, "pixels");
        }

        void RendererHasCorrectOutputDI(ContainerBuilder containerBuilder, string drawType)
        {
            containerBuilder.Register((container, param) => new Triangle(container.Resolve<IRenderer>(), param.Named<int>("size")));
            containerBuilder.Register((container, param) => new Square(container.Resolve<IRenderer>(), param.Named<int>("size")));

            using (var container = containerBuilder.Build())
            {
                Triangle triangle = container.Resolve<Triangle>(new NamedParameter("size", 5));
                Square square = container.Resolve<Square>(new NamedParameter("size", 10));

                Assert.True(triangle.ToString() == $"Drawing Triangle with size 5 as {drawType}.", $"Incorrect output; got: \"{triangle.ToString()}\"");
                Assert.True(square.ToString() == $"Drawing Square with size 10 as {drawType}.", $"Incorrect output; got: \"{square.ToString()}\"");
            }
        }
    }
}
