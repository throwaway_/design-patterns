﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Decorator
{
    public class Lizard : Creature
    {
        public Lizard(string name, int age) : base(name, age) { }
        protected Lizard() : base() { }

        public string Crawl()
        {
            return Age > 1 ? "crawling" : "too young";
        }
    }
}
