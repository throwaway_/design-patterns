﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Decorator
{
    public class Bird : Creature
    {

        public Bird(string name, int age) : base(name, age) { }
        protected Bird() : base() { }

        public string Fly()
        {
            return Age < 10 ? "flying" : "too old";
        }
    }
}
