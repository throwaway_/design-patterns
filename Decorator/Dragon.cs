﻿using System;

namespace Decorator
{
    public class Dragon : Creature
    {
        private Lizard lizard;
        private Bird bird;

        private int age;
        public new int Age
        {
            get
            {
                return age;
            }
            set
            {
                this.age = value;
                this.lizard.Age = value;
                this.bird.Age = value;
            }
        }

        public Dragon(string name, int age)
        {
            this.lizard = new Lizard("inner lizard", age);
            this.bird = new Bird("inner bird", age);

            Name = name;
            Age = age;
        }
        protected Dragon() : base() { }

        public string Fly()
        {
            return this.bird.Fly();
        }
        public string Crawl()
        {
            return this.lizard.Crawl();
        }
    }
}
