﻿using System;
using System.Reflection;

namespace Decorator
{
    public abstract class Creature
    {
        public Creature(string name, int age)
        {
            Name = name;
            Age = Math.Max(age, 0);
        }

        protected Creature()
        {
            Age = 0;
        }

        public string Name;
        public int Age;
        public bool Can(string actionName, out MethodInfo action)
        {
            actionName = actionName.Substring(0,1).ToUpper() + actionName.Substring(1).ToLower();
            action = this.GetType().GetMethod(actionName);
            return action != null;
        }

        private T GiveBirth<T>()
        where T : Creature, new()
        {
            T baby = new T();

            return baby;
        }
    }
}
