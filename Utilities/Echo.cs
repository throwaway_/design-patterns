﻿using NUnit.Framework;
using System;
using System.IO;
using System.Threading;

namespace Utilities
{
    public enum LogLevel
    {
        ERROR,
        WARNING,
        INFO,
        DEBUG
    }

    public static class Echo
    {
        private static string logFolderPath = Path.GetFullPath("..\\..\\..\\logs");
        private static string logFilePath = Path.Combine(logFolderPath, "TestLog.txt");

        public static LogLevel LogLevel;

        static Echo()
        {
            LogLevel = LogLevel.DEBUG;
            if (!Directory.Exists(logFolderPath))
            {
                Directory.CreateDirectory(logFolderPath);
            }
            if (!File.Exists(logFilePath))
            {
                File.Create(logFilePath).Close();
            }
        }

        public static bool TryWrite(LogLevel logLevel, string message)
        {
            if (LogLevel >= logLevel)
            {
                string timestamp = DateTime.Now.ToLocalTime().ToShortTimeString();
                try
                {
                    string formattedMessage = $"[{timestamp}] [{LogLevel.ToString()}] {message}";
                    TestContext.Progress.WriteLine(formattedMessage);
                    using (StreamWriter sw = File.AppendText(logFilePath))
                    {
                        sw.WriteLine(formattedMessage);
                        sw.Flush();
                    }
                }
                catch (Exception ex)
                {
                    TryWrite(LogLevel.ERROR, $"[{timestamp}] [{LogLevel.ToString()}] {ex.InnerException}: {ex.Message}");
                    return false;
                }
            }

            return true;
        }

        public static void Error(string message) { TryWrite(LogLevel.ERROR, message); }
        public static void Warning(string message) { TryWrite(LogLevel.WARNING, message); }
        public static void Info(string message) { TryWrite(LogLevel.INFO, message); }
        public static void Debug(string message) { TryWrite(LogLevel.DEBUG, message); }
    }
}
