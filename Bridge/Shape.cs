﻿using System;

namespace Bridge.Exercise
{
    public abstract class Shape
    {
        public string Name { get; set; }
        public int Size { get; set; }
        public IRenderer Renderer { get; set; }

        public Shape(IRenderer renderer, int size)
        {
            Size = size;
            Renderer = renderer;
        }

        public override string ToString()
        {
            return $"Drawing {Name} with size {Size} as {Renderer.WhatToRenderAs}.";
        }
    }

    public class Triangle : Shape
    {
        public Triangle(IRenderer renderer, int size = 1) : base (renderer, size)
        {
            Name = "Triangle";
        }
    }

    public class Square : Shape
    {
        public Square(IRenderer renderer, int size = 1) : base(renderer, size)
        {
            Name = "Square";
        }
    }
}