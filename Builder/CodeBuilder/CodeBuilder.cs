﻿namespace Builder
{
    public sealed class CodeBuilder : Builder<Code, CodeBuilder>
    {
        public CodeBuilder AddParent(string name)
            => AddAction(code => { code.Root = new CodeComponent(name, "class"); });

        public CodeBuilder AddField(string name, string type)
            => AddAction(code => code.Children.Add(new CodeComponent(name, type)));

        public CodeBuilder(string rootName)
        {
            AddParent(rootName);
        }
    }
}
