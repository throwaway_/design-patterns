﻿using System.Collections.Generic;
using System.Text;

namespace Builder
{
    public class Code
    {
        public CodeComponent Root;
        public List<CodeComponent> Children = new List<CodeComponent>();
        public const int indentSize = 2;

        public override string ToString()
        {
            var lines = new StringBuilder();

            lines.AppendLine($"public {Root.Type} {Root.Name}");
            lines.AppendLine("{");
            foreach (CodeComponent child in Children)
            {
                lines.AppendLine($"{new string(' ', indentSize)}public {child.Type} {child.Name};");
            }
            lines.AppendLine("}");

            return lines.ToString();
        }
    }
}
