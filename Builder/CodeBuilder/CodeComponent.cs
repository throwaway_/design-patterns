﻿using System;

namespace Builder
{
    public class CodeComponent
    {
        public string Name, Type;

        public CodeComponent(string name, string type)
        {
            Name = name ?? throw new ArgumentNullException(paramName: nameof(name));
            Type = type ?? throw new ArgumentNullException(paramName: nameof(type));
        }
    }
}