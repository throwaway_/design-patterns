﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Builder
{
    public abstract class Builder<TSubject, TSelf>
        where TSelf : Builder<TSubject, TSelf>
        where TSubject : new()
    {
        private readonly List<Func<TSubject, TSubject>> actions = new List<Func<TSubject, TSubject>>();
        public TSelf AddAction(Action<TSubject> action)
        {
            actions.Add(subject =>
            {
                action(subject); return subject;
            });
            return (TSelf) this;
        }
        public TSubject Build() => actions.Aggregate(new TSubject(), (subject, function) => function(subject));
    }
}
