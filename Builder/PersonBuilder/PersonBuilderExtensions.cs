﻿namespace Builder.PersonBuilder
{
    public static class PersonBuilderExtensions
    {
        public static PersonBuilder IsAge(this PersonBuilder builder, int age)
            => builder.AddAction((person) => person.Age = age);
    }
}
