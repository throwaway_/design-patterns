﻿namespace Builder.PersonBuilder
{
    public class Person
    {
        public string Name;
        public int Age;

        public override string ToString()
        {
            return $"{Name}: {Age} years old";
        }
    }
}
