﻿namespace Builder.PersonBuilder
{
    public sealed class PersonBuilder : Builder<Person, PersonBuilder>
    {
        public PersonBuilder Called(string name) => AddAction(person => person.Name = name);
        public PersonBuilder IsAge(int age) => AddAction(person => person.Age = age);
    }
}
